# test inserting an svg from repository in readme

*  as `![](data-model-test.svg)`: ![](data-model-test.svg)
*  as `![](https://gitlab.com/dgoo2308/test-project/blob/565760c9c975b37e1ac10cb5c7fdff94c6bd8e9e/data-model-test.svg)`: ![](https://gitlab.com/dgoo2308/test-project/blob/565760c9c975b37e1ac10cb5c7fdff94c6bd8e9e/data-model-test.svg)
*  as `![](https://gitlab.com/dgoo2308/test-project/raw/master/data-model-test.svg)`: ![](https://gitlab.com/dgoo2308/test-project/raw/master/data-model-test.svg)
